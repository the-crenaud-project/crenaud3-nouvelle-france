package entities;

import ai.decisionmaker.BuildingType;
import datastructures.GPoint2D;
import javafx.scene.image.Image;
import physics.PhysicalEntity;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Dormitory extends Building {
    public Dormitory(GPoint2D position, int height, int width) {
        super(position, height, width);
        buildingType = BuildingType.DORMITORY;
        sprite = new Image("/img/buildings/dormitory.png");
        portrait = sprite;
        taskDuration = 4;
    }

    @Override
    protected void collide(PhysicalEntity collidingObject, double lambda){
        if(collidingObject instanceof Character){
            if(!containedSoldiers.contains(collidingObject) && ((Character) collidingObject).getTaskBuildingType() == this.buildingType){
                containedSoldiers.add((Character) collidingObject);
                ((Character) collidingObject).setIsInsideBuilding(true);
            }
        }
    }

    protected void customUpdate(double lambda){

        Iterator<Character> iterator = containedSoldiers.iterator();

        while(iterator.hasNext()){
            Character soldier = iterator.next();
            soldier.sleep(lambda * 5);
            soldier.addTimeInsideBuilding(lambda);
            if(soldier.getTimePassedInsideBuilding() > taskDuration){
                soldier.finishTask(new GPoint2D(position.getX(), getHitbox().getYMax() + 35));
                iterator.remove();
            }
        }

    }
}
