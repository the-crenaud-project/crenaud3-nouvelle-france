package entities;

import ai.decisionmaker.BuildingType;
import ai.pathfinder.Dangerosity;
import datastructures.GPoint2D;
import graphicalengine.Drawable;
import javafx.geometry.Point2D;
import javafx.scene.image.Image;
import ai.pathfinder.DangerosityModifiable;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by aethor on 02/01/18.
 */
public abstract class Building extends GameplayEntity implements Drawable, DangerosityModifiable, ai.decisionmaker.Building{

    protected Image sprite;
    protected BuildingType buildingType;
    protected List<Character> containedSoldiers;
    protected int maxContainedSoldier;
    protected double taskDuration;
    protected List<Dangerosity> dangerosityList;
    private int height;
    private int width;

    public Building(GPoint2D position, int height, int width) {
        super(position, height, width, 0, true, false, false);
        containedSoldiers = new LinkedList<>();
        dangerosityList = new LinkedList<>();
        Dangerosity dangerosity = new Dangerosity((float)width + 20, (float) (height/1.5), Float.POSITIVE_INFINITY);
        dangerosity.setOffset(0, 0);
        dangerosityList.add(dangerosity);
    }

    @Override
    public String getInfo(){
        if(containedSoldiers.size() == 0)
            return buildingType.toString() + "Ce bâtiment ne contient aucun soldat\n";
        else{
            String concat = buildingType.toString() + "Soldats contenus :\n";
            for(Character soldier : containedSoldiers){
                concat = concat + soldier.getNames() + "\n";
            }
            return concat;
        }
    }

    public void evacuate(){
        for(Character soldier : containedSoldiers){
            soldier.finishTask(new GPoint2D(position.getX(), getHitbox().getYMax() + 35));
        }
    }

    @Override
    public Point2D getPosition() {
        return position.toPoint2D();
    }

    @Override
    public GPoint2D getDangerosityPosition(){
        return getHitbox().getCenter();
    }

    @Override
    public float getAngle() {
        return 0;
    }

    @Override
    public List<Dangerosity> getDangerosities() {
        return dangerosityList;
    }

    @Override
    public boolean isTemporary() {
        return false;
    }

    @Override
    public Image getSprite() {
        return sprite;
    }

    @Override
    public boolean isFlippedX() {
        return false;
    }

    @Override
    public boolean isFlippedY() {
        return false;
    }

    @Override
    public BuildingType getBuildingType() {
        return buildingType;
    }

    @Override
    public GPoint2D getEntryPosition() {
        //return new GPoint2D(position.getX(), position.getY() - 65);
        return position;
    }

}
