package entities;

import datastructures.GPoint2D;
import javafx.scene.image.Image;
import physics.PhysicalEntity;

public abstract class GameplayEntity extends PhysicalEntity{

    protected String description;
    protected Image portrait;

    public GameplayEntity(GPoint2D position, int height, int width, double speed, boolean drawable, boolean movable, boolean collidable) {
        super(position, height, width, speed, drawable, movable, collidable);
    }

    public String getInfo(){
        return description;
    }

    public Image getPortrait() {
        return portrait;
    }
}
