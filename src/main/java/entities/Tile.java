package entities;

import datastructures.Entity;
import datastructures.GPoint2D;
import graphicalengine.Drawable;
import javafx.geometry.Point2D;
import javafx.scene.image.Image;

public class Tile extends Entity implements Drawable{

    private Image sprite;
    public static int TILESIZE = 32;

    public Tile(GPoint2D position, Image sprite){
        super(true, position);
        this.sprite = sprite;
    }

    @Override
    public Point2D getPosition() {
        return position.toPoint2D();
    }

    @Override
    public Image getSprite() {
        return sprite;
    }

    @Override
    public boolean isFlippedX() {
        return false;
    }

    @Override
    public boolean isFlippedY() {
        return false;
    }
}
