package entities;

import ai.AIEngine;
import ai.decisionmaker.BuildingType;
import ai.decisionmaker.StateMachine;
import ai.decisionmaker.Task;
import ai.pathfinder.Transport;
import datastructures.GPoint2D;
import graphicalengine.Drawable;
import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import physics.CollisionTable;
import physics.Direction;
import physics.exceptions.NullSpeedException;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Scanner;

public class Character extends GameplayEntity implements Drawable{

    private Image sprite;
    private StateMachine stateMachine;
    private AIEngine aiEngine;

    private float knowledgeLevel;
    private double training;
    private double sleepDeprivation;
    private double hunger;

    private String firstName;
    private String lastName;
    private boolean isBillGertrand;
    private boolean isNapoelonMalaparte;
    private boolean isInsideBuilding;
    private double timePassedInsideBuilding;

    public Character(GPoint2D position, double speed, boolean isBillGertrand, boolean isNapoleonMalaparte, AIEngine aiEngine){
        super(position, 40, 20, speed, true, true, true);
        this.isBillGertrand = isBillGertrand;
        this.isNapoelonMalaparte = isNapoleonMalaparte;
        this.aiEngine = aiEngine;
        isInsideBuilding = false;
        timePassedInsideBuilding = 0;
        this.setCollisionTable(new CollisionTable((byte) 0b11110000, (byte) 0b00001111));

        training = 0;
        knowledgeLevel = 0;
        sleepDeprivation = 0;
        hunger = 0;

        if(isBillGertrand) {
            sprite = new Image("/img/soldiers/heroSoldier.png");
            firstName = "~ Héros";
            lastName = "~";
            description = "Le Héros de cette aventure, plein de courage et d'enthousiasme";
            portrait = new Image("/img/soldiers/heroPortrait.png");
        } else if(isNapoleonMalaparte){
            sprite = new Image("/img/soldiers/napoleonSoldier.png");
            firstName = "Napoléon";
            lastName = "Malaparte";
            description = "Le commandant suprême de la Nouvelle France";
            portrait = new Image("/img/soldiers/napoleonPortrait.png");
        }else{
            sprite = new Image("/img/soldiers/soldier.png");
            portrait = new Image("/img/soldiers/soldierPortrait.png");
            description = "Un brave soldat de la Nouvelle France";
            training = 0;
            try {
                firstName = generateFirstName();
                lastName = generateLastName();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        stateMachine = new StateMachine(new Task(1, BuildingType.getRandomBuildingType()), Transport.PEDESTRIAN);
    }

    private String generateFirstName() throws IOException {
        String[] splitted = getSplittedStringFromFile("/datas/firstNames", ":");
        return splitted[(int) (Math.random() * (splitted.length - 1))];
    }

    private String generateLastName() throws IOException {
        String[] splitted = getSplittedStringFromFile("/datas/lastNames", ":");
        return splitted[(int) (Math.random() * (splitted.length - 1))];
    }

    private String[] getSplittedStringFromFile(String filePath, String splitPattern) throws IOException {
        Scanner scanner = new Scanner(getClass().getResourceAsStream(filePath));
        String line = scanner.useDelimiter("\\Z").nextLine();
        String[] splitted = line.split(splitPattern);
        return splitted;
    }

    @Override
    public String getInfo(){
        return firstName + " " + lastName + "\n" + description + "\nentrainement : " + (int) training
                + "\nconnaissance : " + knowledgeLevel * 100 + "/100"
                + "\nmanque de sommeil : " + (int) sleepDeprivation + "/100"
                + "\nfaim : " + (int) hunger + "/100";
    }

    public String getNames(){
        return  firstName + " " + lastName;
    }

    @Override
    public Image getSprite() {
        return isInsideBuilding?null:sprite;
    }

    @Override
    public boolean isFlippedX() {
        return false;
    }

    @Override
    public boolean isFlippedY() {
        return false;
    }

    @Override
    public Point2D getPosition(){
        return position.toPoint2D();
    }

    @Override
     protected void customUpdate(double lambda){
        //stateMachine.updateState(aiEngine, position, knowledgeLevel);
        stateMachine.updateState(aiEngine, getHitbox().getCenter(), knowledgeLevel);
        Direction newDirection = stateMachine.getTarget(position, (float) 1);
        if(newDirection != null) {
            try {
                //System.out.println(getNames()+"=>"+newDirection);
                move(newDirection);
            } catch (NullSpeedException e) {
                e.printStackTrace();
            }
        }

        sleepDeprivation += lambda;
        hunger += lambda;
    }

    public void finishTask(GPoint2D newPosition){
         //terminate task
         moveTo(newPosition);//get away from building
         stateMachine.doTask(stateMachine.getCurrentTask().getTarget());
         isInsideBuilding = false;
         timePassedInsideBuilding = 0;
         chooseNewTask();
    }

    private void chooseNewTask(){
        stateMachine.tasks = new LinkedList<>();
        stateMachine.addTask(new Task(2, BuildingType.getRandomBuildingType()));
        stateMachine.updateState(aiEngine, position, knowledgeLevel);
    }

    public void forceNewTask(Task task){
        stateMachine.doTask(stateMachine.getCurrentTask().getTarget());
        stateMachine.tasks = new LinkedList<>();
        stateMachine.addTask(task);
        stateMachine.updateState(aiEngine, position, knowledgeLevel);
    }

    public void addTraining(int addedTraining){
        training += addedTraining * (1 + knowledgeLevel) + (isBillGertrand?addedTraining:0);
        sleepDeprivation += 0.1;
    }

    public double getTraining(){
        return training;
    }

    public void sleep(double sleepAmount){
        sleepDeprivation -= sleepAmount;
        sleepDeprivation = sleepDeprivation<0?0:sleepDeprivation;
    }

    public void eat(double amount){
        hunger -= amount;
        hunger = hunger<0?0:hunger;
    }

    public void addKnowledgeLevel(double amount){
        knowledgeLevel += amount/(1 + 2*knowledgeLevel);
        knowledgeLevel = knowledgeLevel>1?1:knowledgeLevel;
    }

    public BuildingType getTaskBuildingType(){
        return stateMachine.getCurrentTask().getTarget();
    }


    public double getTimePassedInsideBuilding(){
        return timePassedInsideBuilding;
    }

    public void addTimeInsideBuilding(double time){
        timePassedInsideBuilding += time;
    }

    public boolean isInsideBuilding(){
        return isInsideBuilding;
    }

    public void setIsInsideBuilding(boolean inside){
        isInsideBuilding = inside;
    }

    public boolean shouldDie(){
        return ((sleepDeprivation>=100 || hunger>=100)&& !isInsideBuilding());
    }

    public boolean isCritical(){
        return ((sleepDeprivation>=80 || hunger>=80));
    }

}
