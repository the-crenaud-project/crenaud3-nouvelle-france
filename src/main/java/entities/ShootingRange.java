package entities;

import ai.decisionmaker.BuildingType;
import datastructures.GPoint2D;
import javafx.scene.image.Image;
import physics.PhysicalEntity;

import java.util.Iterator;

public class ShootingRange extends Building{
    public ShootingRange(GPoint2D position, int height, int width) {
        super(position, height, width);
        buildingType = BuildingType.SHOOTINGRANGE;
        sprite = new Image("/img/buildings/shootingrange.png");
        portrait = sprite;
        taskDuration = 3;
    }

    @Override
    protected void collide(PhysicalEntity collidingObject, double lambda){
        if(collidingObject instanceof Character){
            if(!containedSoldiers.contains(collidingObject) &&
                    ((Character) collidingObject).getTaskBuildingType() == this.buildingType){
                containedSoldiers.add((Character) collidingObject);
                ((Character) collidingObject).setIsInsideBuilding(true);
            }
        }
    }

    protected void customUpdate(double lambda){

        Iterator<Character> iterator = containedSoldiers.iterator();

        while(iterator.hasNext()){
            Character soldier = iterator.next();
            soldier.addTraining(1);
            soldier.addTimeInsideBuilding(lambda);
            if(soldier.getTimePassedInsideBuilding() > taskDuration){
                soldier.addKnowledgeLevel(0.1);
                soldier.finishTask(new GPoint2D(position.getX(), getHitbox().getYMax() + 35));
                iterator.remove();
            }
        }

    }
}
