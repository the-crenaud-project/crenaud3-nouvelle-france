package scenes;

import ai.AIEngine;
import ai.decisionmaker.BuildingType;
import ai.decisionmaker.Task;
import ai.pathfinder.Dangerosity;
import ai.pathfinder.DangerosityModifiable;
import ai.pathfinder.Graph;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import datastructures.Entity;
import datastructures.GPoint2D;
import datastructures.Grid;
import entities.*;
import entities.Character;
import graphicalengine.Drawable;
import graphicalengine.ResizableCanvas;
import javafx.animation.PauseTransition;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.util.Duration;
import physics.CollisionTable;
import physics.Direction;
import physics.PhysicalEntity;
import physics.World;
import physics.exceptions.NullSpeedException;
import scenecontroller.Scene;
import scenecontroller.SceneController;
import graphicalengine.Camera;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by aethor on 08/12/17.
 */
public class Level extends Scene{

    @FXML public ResizableCanvas canvas;
    @FXML public Text infoText;
    @FXML public GridPane actionGrid;
    @FXML public ImageView portrait;
    @FXML public Button eatButton;
    @FXML public Button sleepButton;
    @FXML public Button shootButton;
    @FXML public Button evacuateButton;

    private Camera camera;
    private World world;
    private PauseTransition gameLoop;
    private AIEngine aiEngine;

    private double frameDuration;
    private Entity selectedObject;
    private boolean isPaused = true;
    private GPoint2D worldDimensions;
    private int soldierNumber;
    private int score;
    private boolean debug;
    private double timeElapsed;
    private double maxTime;

    public Level(){
        fxmlPath = "fxml/level.fxml";
        cssPath = "css/level.css";
        frameDuration = 0.03;
        soldierNumber = 10;
        score = 0;
        timeElapsed = 0;
        maxTime = 180;
    }

    @Override
    public void play(SceneController controller, InputStream configFile){
        debug = false;
        super.play(controller, configFile);

        try{
            this.loadConfigFile(configFile);
        } catch (IOException e){
            e.getMessage();
            e.printStackTrace();
        }

        worldDimensions = new GPoint2D(1024, 1024);
        camera = new Camera(canvas);
        world = World.getInstance((int) worldDimensions.getX(), (int) worldDimensions.getY());
        aiEngine = new AIEngine((float) worldDimensions.getX(), (float) worldDimensions.getY(), (float) 16);

        //Background loading : experimental
        Image tileImage = new Image("/img/backgrounds/sand.png");
        for(int i = 0; i <= worldDimensions.getX() * 2; i += Tile.TILESIZE){
            for(int j = 0; j <= worldDimensions.getY() * 2; j += Tile.TILESIZE){
                Tile tile = new Tile(new GPoint2D(i, j), tileImage);
                camera.addBackground(tile, 1);
            }
        }

        //Soldier loading
        for(int i = 0; i < soldierNumber - 2; i++){
            Character character = new Character(new GPoint2D(40 + (worldDimensions.getX())/soldierNumber * i, 200), 50,
                    false, false, aiEngine);
            world.add(character);
        }
        world.add(new Character(new GPoint2D(100, 100), 40, true, false, aiEngine));//Bill
        world.add(new Character(new GPoint2D(200, 100), 60, false, true, aiEngine));//Napoléon

        //Building loading
        Building building1 = new Canteen(new GPoint2D(100, 475),100, 80);
        addBuilding(building1);
        Building building2 = new Dormitory(new GPoint2D(400, 400), 100, 80);
        addBuilding(building2);
        Building building3 = new ShootingRange(new GPoint2D(600, 450), 100, 80);
        addBuilding(building3);


        isPaused = false;

        this.gameLoop = new PauseTransition(Duration.seconds(frameDuration));
        this.gameLoop.setOnFinished(e-> {
            this.gameLoop();
            gameLoop.playFromStart();
        });
        gameLoop.play();

    }


    private void loadConfigFile(InputStream configFile) throws IOException{
        Gson json = new Gson();
        JsonReader reader = json.newJsonReader(new InputStreamReader(configFile));

        reader.beginObject();

        String objectName;
        while (reader.hasNext()){
            objectName = reader.nextName();
            switch (objectName){
                case"test":
                    System.out.println("test");
                    break;
                case "soldierNumber":
                    int readInt = reader.nextInt();
                    soldierNumber = readInt>2?readInt:2;
                    break;
                case "debug":
                    debug = reader.nextBoolean();
                    break;
                case "maxTime":
                    maxTime = reader.nextDouble();
                    break;
                default:
                    reader.skipValue();
                    break;
            }
        }
    }

    private void addBuilding(Building building){
        world.add(building);
        aiEngine.register(building);
    }

    private void gameLoop(){

        canvas.requestFocus();//allow key event capture
        if(!isPaused){

            GraphicsContext gc = canvas.getGraphicsContext2D();

            //general drawing
            camera.draw(world.getGrid().getDrawables());

            //info text drawing
            getGrid().getEntities().forEach(entity ->{
                if(entity instanceof Character){
                    if(!((Character) entity).isInsideBuilding()){//dont display of character is in a building
                        Point2D correctedPosition = new Point2D(((Character) entity).getPosition().getX() - camera.getPosition().getX(),
                                ((Character) entity).getPosition().getY() - camera.getPosition().getY());
                        if(entity == selectedObject)
                            gc.setFill(Color.RED);
                        else
                            gc.setFill(Color.BLACK);
                        String name = ((Character) entity).getNames();
                        gc.fillText(((Character) entity).getNames(),
                                correctedPosition.getX() - 10 - name.length(),
                                correctedPosition.getY() - 10);
                    }

                }
            });

            score = 0;
            for(Entity entity : getGrid().getEntities()){
                if(entity instanceof Character)
                    score += ((Character) entity).getTraining();
            }
            gc.setFill(Color.BLACK);
            gc.fillText("Score total : " + String.valueOf(score), 25, 25);

            //Affichage du temps
            gc.fillText("Temps restant : " + String.valueOf((int) (maxTime - timeElapsed)), 25, 50);

            //entity status gestion
            int criticalEntities = 0;
            for(Entity entity  : getGrid().getEntities()){
                if(entity instanceof Character){
                    if(((Character) entity).isCritical()){ //send message if entity status is critical
                        criticalEntities++;
                        gc.fillText(((Character) entity).getNames() + " est en état critique !", 25, 50 + 25 * criticalEntities);
                    }
                    if(((Character) entity).shouldDie()){ //kill entity
                        world.remove(entity);
                        camera.addBackground(new Tile(((Character) entity).getPhysicalPosition(), new Image("/img/backgrounds/blood.png")),2);
                    }
                }
            }

            updateUI();

            //debug hitbox and dangerosities printing
            if(debug){
                for(Entity entity : getGrid().getEntities()){
                    if(entity instanceof Character){
                        PhysicalEntity.Hitbox hitbox = ((Character) entity).getHitbox();
                        gc.strokeRect(hitbox.getX() - camera.getPosition().getX(),
                                hitbox.getY() - camera.getPosition().getY(),
                                hitbox.getXMax() - hitbox.getX(), hitbox.getYMax() - hitbox.getY());
                    }else if(entity instanceof Building){
                        //GPoint2D pos = ((Building) entity).getDangerosityPosition();
                        Dangerosity dan = ((Building) entity).getDangerosities().get(0);
                        gc.strokeRect(dan.getCorner((DangerosityModifiable) entity).getX() - camera.getPosition().getX(),
                                dan.getCorner((DangerosityModifiable) entity).getY() - camera.getPosition().getY(),
                                (double) ((DangerosityModifiable) entity).getDangerosities().get(0).getWidth(),
                                (double) ((DangerosityModifiable) entity).getDangerosities().get(0).getHeight());
                    }
                }
            }


            //world update
            this.world.update(frameDuration);
            timeElapsed += frameDuration;
            if(timeElapsed >= maxTime){
                isPaused = true;
            }
        }

    }

    public void keyPressed(KeyEvent event){
        switch (event.getCode()){
            case Z:
                camera.move(new Point2D(0, -8));
                break;
            case S:
                camera.move(new Point2D(0, 8));
                break;
            case Q:
                camera.move(new Point2D(-8, 0));
                break;
            case D:
                camera.move(new Point2D(8, 0));
                break;
            case ESCAPE:
                isPaused = !isPaused;
                break;
        }
    }

    public void scrollUsed(ScrollEvent event){
        if(event.getDeltaY() > 0)
            camera.multiplyZoom((float) 1.2);
        else
            camera.multiplyZoom((float) 0.8);
    }

    public void characterSleepPressed(){
        if(selectedObject != null && selectedObject instanceof Character){
            ((Character) selectedObject).forceNewTask(new Task(2, BuildingType.DORMITORY));
        }
    }

    public void characterShootPressed(){
        if(selectedObject != null && selectedObject instanceof Character){
            ((Character) selectedObject).forceNewTask(new Task(2, BuildingType.SHOOTINGRANGE));
        }
    }

    public void characterEatPressed(){
        if(selectedObject != null && selectedObject instanceof Character){
            ((Character) selectedObject).forceNewTask(new Task(2, BuildingType.CANTEEN));
        }
    }

    public void buildingEvacuatePressed(){
        if(selectedObject instanceof Building){
            ((Building) selectedObject).evacuate();
        }
    }


    /**
     * Used to select characters
     * @param event
     */
    public void mouseClicked(MouseEvent event){
        Point2D cameraMousePosition = camera.fromCanvasToCameraView(new Point2D(event.getX(), event.getY()));
        selectedObject = world.getGrid().getClosest(GPoint2D.toGpoint2D(cameraMousePosition), 100);
        if(selectedObject instanceof Character){
            selectedObject = ((Character) selectedObject).isInsideBuilding()?null:selectedObject;
        }

    }

    private void updateUI(){
        if(selectedObject == null){
            portrait.setImage(null);
            showCharactersButton(false);
            showBuildingsButton(false);
            infoText.setText("-");
        }else if(selectedObject instanceof GameplayEntity){
            this.portrait.setImage(((GameplayEntity) selectedObject).getPortrait());
            infoText.setText(((GameplayEntity) selectedObject).getInfo());
            if(selectedObject instanceof Character){
                if(((Character) selectedObject).isInsideBuilding()){
                    selectedObject = null;
                }else{
                    showCharactersButton(true);
                    showBuildingsButton(false);
                }
            }else if(selectedObject instanceof Building){
                showCharactersButton(false);
                showBuildingsButton(true);
            }

        }

    }

    private void showCharactersButton(boolean show){
        shootButton.setVisible(show);
        eatButton.setVisible(show);
        sleepButton.setVisible(show);
    }

    private void showBuildingsButton(boolean show){
        evacuateButton.setVisible(show);
    }


    @Override
    public void end() {
        if (this.controller != null)
            this.controller = null;
        if (gameLoop != null ) {
            gameLoop.setOnFinished(e -> {
            });
            gameLoop.stop();
        }
    }

    private Grid getGrid(){
        return world.getGrid();
    }

}
