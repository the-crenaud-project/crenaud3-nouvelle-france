package scenes;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import scenecontroller.Scene;
import scenecontroller.SceneController;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by aethor on 08/12/17.
 * Basic menu
 */
public class Menu extends Scene{

    @FXML private ImageView background;
    @FXML private Button playButton;
    @FXML private Button exitButton;


    /**
     * default Menu constructor
     */
    public Menu(){
        fxmlPath = "fxml/menu.fxml";
        cssPath = "css/menu.css";
    }

    @Override
    public void play(SceneController controller, InputStream configFile){
        super.play(controller, configFile);

        try {
            this.loadConfigFile(configFile);
        } catch (IOException e) {
            e.getMessage();
            e.printStackTrace();
        }

    }

    private void loadConfigFile(InputStream configFile) throws IOException{
        Gson json = new Gson();
        JsonReader reader = json.newJsonReader(new InputStreamReader(configFile));

        reader.beginObject();

        String objectName;
        while (reader.hasNext()){
            objectName = reader.nextName();
            switch (objectName){
                case "background":
                    try{
                        Image backgroundImage = new Image(reader.nextString());
                        background.setImage(backgroundImage);
                    }
                    catch (IllegalArgumentException e){
                        e.printStackTrace();
                        System.out.println("crenaud3 : warning : couldn't find menu's background image");
                    }

                    break;
                default:
                    reader.skipValue();
                    break;
            }

        }

        reader.endObject();
    }

    @Override
    public void end() {
        if(controller != null)
            controller = null;
    }

    public void handlePlayAction(ActionEvent actionEvent) {
        playButton.setDisable(true);
        exitButton.setDisable(true);
        controller.sceneEnded();
    }

    public void handleExitAction(ActionEvent actionEvent) {
        System.exit(0);
    }
}
