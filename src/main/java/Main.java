import javafx.application.Application;
import javafx.stage.Stage;
import scenecontroller.SceneController;

public class Main extends Application {

    private Stage stage;
    private SceneController controller;

    public static void main(String[] args){
        launch(args);
    }


    public void start(Stage primaryStage) throws Exception {

        controller = new SceneController("/controller.godwin");
        stage = primaryStage;
        controller.play(stage);
    }
}
