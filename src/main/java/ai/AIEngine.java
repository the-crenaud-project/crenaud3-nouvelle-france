package ai;

import ai.decisionmaker.Building;
import ai.pathfinder.Dangerosity;
import ai.pathfinder.*;
import datastructures.GPoint2D;
import javafx.scene.canvas.GraphicsContext;

import java.util.*;

public class AIEngine {
    protected Graph pedestrian;
    protected Graph vehicule;
    protected List<DangerosityModifiable> dangerosity;
    protected List<Building> building;

    public AIEngine(float w, float h, float caseSize){
        int width=(int)Math.ceil(w/caseSize),height=(int)Math.ceil(h/caseSize);
        pedestrian=new Graph(width,height,caseSize);
        vehicule=new Graph(width,height,caseSize);
        dangerosity=new LinkedList<DangerosityModifiable>();
        building=new LinkedList<Building>();
    }

    public void register(Object o){
        if(o instanceof Building){
            register((Building)o);
        }
        if(o instanceof DangerosityModifiable){
            register((DangerosityModifiable)o);
        }
    }

    protected void register(Building b){
        building.add(b);
    }

    protected void register(DangerosityModifiable dm){
        dangerosity.add(dm);
        if(!dm.isTemporary()) {
            List<Dangerosity> lst = dm.getDangerosities();
            GPoint2D pos = dm.getDangerosityPosition();
            float angle = dm.getAngle();
            for (Dangerosity d : lst) {
                Transport t = d.getType();
                if (t == Transport.PEDESTRIAN || t == Transport.BOTH) {
                    d.modifyWeight(pedestrian, (float) pos.getX(), (float) pos.getY(), angle, false);
                }
                if (t == Transport.VEHICULE || t == Transport.BOTH) {
                    d.modifyWeight(vehicule, (float) pos.getX(), (float) pos.getY(), angle, false);
                }
            }
        }
    }
    public void unregister(Object o){
        if(o instanceof Building){
            unregister((Building) o);
        }
        if(o instanceof DangerosityModifiable){
            unregister((DangerosityModifiable) o);
        }
    }

    protected void unregister(Building b){
        building.remove(b);
    }

    protected void unregister(DangerosityModifiable dm){
        dangerosity.remove(dm);
        if(!dm.isTemporary()) {
            List<Dangerosity> lst = dm.getDangerosities();
            GPoint2D pos = dm.getDangerosityPosition();
            float angle = dm.getAngle();
            for (Dangerosity d : lst) {
                Transport t = d.getType();
                if (t == Transport.PEDESTRIAN || t == Transport.BOTH) {
                    d.modifyWeight(pedestrian, (float) pos.getX(), (float) pos.getY(), angle, false,1.0f);
                }
                if (t == Transport.VEHICULE || t == Transport.BOTH) {
                    d.modifyWeight(vehicule, (float) pos.getX(), (float) pos.getY(), angle, false,1.0f);
                }
            }
        }
    }

    public List<Building> getBuilding(){
        return building;
    }

    public void update(){
        pedestrian.clearTemporaryWeight();
        vehicule.clearTemporaryWeight();
        for(DangerosityModifiable dm : dangerosity){
            if(dm.isTemporary()) {
                List<Dangerosity> lst = dm.getDangerosities();
                GPoint2D pos = dm.getDangerosityPosition();
                float angle = dm.getAngle();
                for (Dangerosity d : lst) {
                    Transport t = d.getType();
                    if (t == Transport.PEDESTRIAN || t == Transport.BOTH) {
                        d.modifyWeight(pedestrian, (float) pos.getX(), (float) pos.getY(), angle, true);
                    }
                    if (t == Transport.VEHICULE || t == Transport.BOTH) {
                        d.modifyWeight(vehicule, (float) pos.getX(), (float) pos.getY(), angle, true);
                    }
                }
            }
        }
    }

    public List<Node> getPath(Node start,Node goal,float knowledgeLevel,Transport t){
        if(t==Transport.PEDESTRIAN || t==Transport.BOTH){
            return pedestrian.aStar(start,goal,knowledgeLevel);
        }else if(t==Transport.VEHICULE){
            return vehicule.aStar(start,goal,knowledgeLevel);
        }
        return null;
    }

    public List<GPoint2D> getPath(GPoint2D start,GPoint2D goal,float knowledgeLevel,Transport t){
        float nodeWidth=t==Transport.VEHICULE?vehicule.getNodeWidth():pedestrian.getNodeWidth();
        Node s=new Node((int)Math.round(start.getX()/nodeWidth),(int)Math.round(start.getY()/nodeWidth));
        Node g=new Node((int)Math.round(goal.getX()/nodeWidth),(int)Math.round(goal.getY()/nodeWidth));
        List<Node> path=getPath(s,g,knowledgeLevel,t);//find path
        if(path==null){
            return null;
        }

        List<GPoint2D> result=new ArrayList<>();//convert the result
        result.add(start);
        for(Node n : path){
            result.add(new GPoint2D(n.x*nodeWidth,n.y*nodeWidth));
        }
        result.add(goal);

        if(result.size()>2) {//simplify path
            GPoint2D p0=result.get(0);
            GPoint2D p1=result.get(1);
            GPoint2D p2=result.get(2);
            if((p0.distance(p1)+p1.distance(p2))>p0.distance(p2)){
                result.remove(1);
            }
        }
        if(result.size()>2) {
            GPoint2D p0=result.get(result.size()-1);
            GPoint2D p1=result.get(result.size()-2);
            GPoint2D p2=result.get(result.size()-3);
            if((p0.distance(p1)+p1.distance(p2))>p0.distance(p2)){
                result.remove(result.size()-2);
            }
        }
        return result;
    }

    public Graph getPedestrianGraph() {
        return pedestrian;
    }

    /*public static void drawDebug(GraphicsContext gc){
        float caseSize=1.0f;
        AIEngine aiEngine =new AIEngine(20,20,caseSize);
        long t0=System.nanoTime();
        Test test1 = new Test();
        test1.danger.add(new Dangerosity(5, 5, Float.POSITIVE_INFINITY));
        test1.danger.add(new Dangerosity(5, 5, 10).setOffset(0, 5));
        test1.pos.setLocation(5, 5);
        aiEngine.register(test1);
        Test test2 = new Test();
        test2.temporary = true;
        test2.danger.add(new Dangerosity(2, 2, 5));
        test2.pos.setLocation(10, 10);
        aiEngine.register(test2);
        Test test3 = new Test();
        test3.temporary = true;
        test3.danger.add(new Dangerosity(2, 2, 5));
        test3.pos.setLocation(11, 10);
        aiEngine.register(test3);
        Test test4 = new Test();
        test4.temporary = true;
        test4.danger.add(new Dangerosity(2, 2, 5));
        test4.pos.setLocation(8, 8);
        aiEngine.register(test4);
        Test test5 = new Test();
        test5.danger.add(new Dangerosity(2, 2, Float.POSITIVE_INFINITY));
        test5.pos.setLocation(1, 1);
        aiEngine.register(test5);
        aiEngine.update();
        test2.pos.setLocation(12, 15);
        test4.pos.setLocation(12, 7);
        aiEngine.update();
        aiEngine.unregister(test5);
        aiEngine.update();
        long t1=System.nanoTime();
        System.out.println("durration:"+(t1-t0)*1e-6);

        Graph.drawGraphWeight(aiEngine.vehicule,gc);
    }*/
}
