package ai.pathfinder;

public class Node{
    public int x;
    public int y;
    protected float heuristicCost=Float.POSITIVE_INFINITY;
    public Node(int x,int y){
        this.x=x;
        this.y=y;
    }
    public Node(int x,int y,float hc){
        this.x=x;
        this.y=y;
        this.heuristicCost=hc;
    }
}