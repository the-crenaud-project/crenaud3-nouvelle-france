package ai.pathfinder;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.*;

public class Graph{
    protected float[][] weights;
    protected float[][] temporaryWeights;
    protected float[][] cost;
    protected Node goal;
    protected float nodeWidth;

    public Graph(int width,int height,float nodeWidth){
        weights=new float[width][height];
        for(int x=0;x<width;x++){
            for(int y=0;y<height;y++){
                weights[x][y]=1.0f;
            }
        }
        temporaryWeights=new float[width][height];
        cost=new float[width][height];
        this.nodeWidth=nodeWidth;
        this.clearTemporaryWeight();
    }

    public float getNodeWidth(){
        return nodeWidth;
    }

    public int getWidth(){
        return weights.length;
    }

    public int getHeight(){
        return weights[0].length;
    }

    private void resetCost(){
        for(int x=0;x<cost.length;x++){
            for(int y=0;y<cost[0].length;y++){
                cost[x][y]=Float.POSITIVE_INFINITY;
            }
        }
    }

    public void clearTemporaryWeight(){
        for(int x=0;x<temporaryWeights.length;x++){
            for(int y=0;y<temporaryWeights[0].length;y++){
                temporaryWeights[x][y]=weights[x][y];
            }
        }
    }

    public void setNodeWeight(int x,int y,float w){
        setNodeWeight(x,y,w,false);
    }

    public void setNodeWeight(int x,int y,float w,boolean tmp){
        if(!tmp) {
            weights[x][y] = w;
        }
        temporaryWeights[x][y] = w;
    }

    public static void display(float[][] mat){
        float min=Float.POSITIVE_INFINITY,max=Float.NEGATIVE_INFINITY;
        for(int x=0;x<mat.length;x++){
            for(int y=0;y<mat[0].length;y++){
                if(mat[x][y]<min && mat[x][y]!=Float.NEGATIVE_INFINITY){
                    min=mat[x][y];
                }
                if(mat[x][y]>max && mat[x][y]!=Float.POSITIVE_INFINITY){
                    max=mat[x][y];
                }
            }
        }
        for(int x=0;x<mat.length;x++){
            for(int y=0;y<mat[0].length;y++){
                if(mat[x][y]!=Float.POSITIVE_INFINITY && mat[x][y]!=Float.NEGATIVE_INFINITY) {
                    System.out.print((int) ((mat[x][y] - min) / (max - min) * 9.9999));
                }else{
                    System.out.print("#");
                }
            }
            System.out.println("");
        }
    }

    protected static float weightModifier(float w,float knowledg){
        return knowledg*w+1-knowledg;
    }

    public List<Node> aStar(Node start,Node goal,float knoledgLevel){
        this.resetCost();
        this.goal=goal;
        TreeSet<Node> tree=new TreeSet<Node>(new NodeCompare());
        List<Node> lst=new LinkedList<Node>();
        cost[start.x][start.y]=0;
        tree.add(new Node(start.x,start.y,heuristic(start)));
        while(!tree.isEmpty()){
            Node current=tree.pollFirst();
            if(current.x==this.goal.x && current.y==this.goal.y){
                lst.add(current);
                while(current.x!=start.x || current.y!=start.y){
                    List<Node> neighbour=getNeighbour(current);
                    float min=Float.POSITIVE_INFINITY;
                    for(Node n : neighbour){
                        if(cost[n.x][n.y]<min){
                            min=cost[n.x][n.y];
                            current=n;
                        }
                    }
                    lst.add(current);
                }
                Collections.reverse(lst);
                return lst;
            }
            for(Node neighbour : getNeighbour(current)){
                float neighbourWeight=weightModifier(temporaryWeights[neighbour.x][neighbour.y],knoledgLevel);
                float newCost=neighbourWeight+cost[current.x][current.y];
                if(newCost<cost[neighbour.x][neighbour.y] && neighbourWeight!=Float.POSITIVE_INFINITY) {
                    cost[neighbour.x][neighbour.y] = newCost;
                    tree.add(new Node(neighbour.x,neighbour.y,heuristic(neighbour)));
                }
            }
        }
        return null;
    }

    public List<Node> getNeighbour(Node n){
        List<Node> neighbour=new LinkedList<Node>();
        if(n.x>0){
            neighbour.add(new Node(n.x-1,n.y));
        }
        if(n.x<(temporaryWeights.length-1)){
            neighbour.add(new Node(n.x+1,n.y));
        }
        if(n.y>0){
            neighbour.add(new Node(n.x,n.y-1));
        }
        if(n.y<(temporaryWeights[0].length-1)){
            neighbour.add(new Node(n.x,n.y+1));
        }
        return neighbour;
    }

    public float heuristic(Node n) {
        return 2.0f*Double.valueOf(Math.sqrt(Math.pow(this.goal.x - n.x, 2)+Math.pow(this.goal.y-n.y,2))).floatValue();
        //return Double.valueOf(Math.pow(this.goal.x - n.x, 2)+Math.pow(this.goal.y-n.y,2)).floatValue();
        //return Math.abs(this.goal.x-n.x)+Math.abs(this.goal.y-n.y);
    }

    class NodeCompare implements Comparator<Node> {
        @Override
        public int compare(Node n1, Node n2) {
            if((cost[n1.x][n1.y]+n1.heuristicCost)<(cost[n2.x][n2.y]+n2.heuristicCost)){
                return -1;
            }
            if((cost[n1.x][n1.y]+n1.heuristicCost)>(cost[n2.x][n2.y]+n2.heuristicCost)){
                return 1;
            }
            if(n1.x<n2.x){
                return -1;
            }
            if(n1.x>n2.x){
                return 1;
            }
            if(n1.y<n2.y){
                return -1;
            }
            if(n1.y>n2.y){
                return 1;
            }
            return 0;
        }
    }
    public static void drawDebug(GraphicsContext gc){
        float caseSize= (float) (gc.getCanvas().getWidth()/20.0);
        Graph graph = new Graph(20, 20,1.0f);
        graph.setNodeWeight(10, 10, Float.POSITIVE_INFINITY);
        graph.setNodeWeight(9, 9, Float.POSITIVE_INFINITY);
        graph.setNodeWeight(9, 11, Float.POSITIVE_INFINITY);
        graph.setNodeWeight(8, 8, Float.POSITIVE_INFINITY,true);
        graph.setNodeWeight(8, 12, Float.POSITIVE_INFINITY);
        graph.setNodeWeight(7, 7, Float.POSITIVE_INFINITY);
        graph.setNodeWeight(7, 13, Float.POSITIVE_INFINITY);
        graph.setNodeWeight(6, 6, Float.POSITIVE_INFINITY);
        graph.setNodeWeight(6, 14, Float.POSITIVE_INFINITY);
        graph.setNodeWeight(5, 5, Float.POSITIVE_INFINITY);
        graph.setNodeWeight(5, 15, Float.POSITIVE_INFINITY);
        graph.setNodeWeight(4, 4, 1.5f);
        graph.setNodeWeight(4, 16, 2.0f);
        graph.clearTemporaryWeight();
        List<Node> lst=graph.aStar(new Node(0, 10), new Node(19, 10),1.0f);

        float min=Float.POSITIVE_INFINITY,max=Float.NEGATIVE_INFINITY;
        for(int x=0;x<20;x++) {
            for (int y = 0; y < 20; y++) {
                if(graph.weights[x][y]!=Float.POSITIVE_INFINITY){
                    if(graph.weights[x][y]<min){
                        min=graph.weights[x][y];
                    }
                    if(graph.weights[x][y]>max){
                        max=graph.weights[x][y];
                    }
                }
            }
        }
        double w=2;
        gc.setLineWidth(w);
        for(int x=0;x<20;x++){
            for(int y=0;y<20;y++){
                gc.setStroke(graph.cost[x][y]!=Float.POSITIVE_INFINITY? Color.GREEN:Color.BLACK);
                gc.setFill(graph.weights[x][y]==Float.POSITIVE_INFINITY?Color.RED:Color.color(1.0f-(graph.weights[x][y]-min)/(max-min),1.0f-(graph.weights[x][y]-min)/(max-min),1.0f-(graph.weights[x][y]-min)/(max-min)));
                gc.fillRect(x*caseSize+w,y*caseSize+w,caseSize-w,caseSize-w);
                gc.strokeRect(x*caseSize+w,y*caseSize+w,caseSize-w,caseSize-w);
            }
        }
        gc.setStroke(Color.BLUE);
        gc.setLineWidth(5);
        Node prev=lst.get(0);
        lst.remove(0);
        for(Node n : lst){
            gc.strokeLine((prev.x+0.5)*caseSize,(prev.y+0.5)*caseSize,(n.x+0.5)*caseSize,(n.y+0.5)*caseSize);
            prev=n;
        }
    }
    public static void drawGraphWeight(Graph graph,GraphicsContext gc){
        float caseSize= (float) (gc.getCanvas().getWidth()/graph.getWidth());

        float min=Float.POSITIVE_INFINITY,max=Float.NEGATIVE_INFINITY;
        for(int x=0;x<20;x++) {
            for (int y = 0; y < 20; y++) {
                if(graph.temporaryWeights[x][y]!=Float.POSITIVE_INFINITY){
                    if(graph.temporaryWeights[x][y]<min){
                        min=graph.temporaryWeights[x][y];
                    }
                    if(graph.temporaryWeights[x][y]>max){
                        max=graph.temporaryWeights[x][y];
                    }
                }
            }
        }
        if(min==max){
            max=min+1;
        }
        double w=2;
        gc.setLineWidth(w);
        for(int x=0;x<20;x++){
            for(int y=0;y<20;y++){
                gc.setStroke(Color.BLACK);
                gc.setFill(graph.temporaryWeights[x][y]==Float.POSITIVE_INFINITY?Color.RED:Color.color(1.0f-(graph.temporaryWeights[x][y]-min)/(max-min),1.0f-(graph.temporaryWeights[x][y]-min)/(max-min),1.0f-(graph.temporaryWeights[x][y]-min)/(max-min)));
                gc.fillRect(x*caseSize+w,y*caseSize+w,caseSize-w,caseSize-w);
                gc.strokeRect(x*caseSize+w,y*caseSize+w,caseSize-w,caseSize-w);
            }
        }
    }
    public static void drawGraphWeight(Graph graph){
        for(int y=0;y<graph.getHeight();y++){
            for(int x=0;x<graph.getWidth();x++){
                if(graph.temporaryWeights[x][y]==Float.POSITIVE_INFINITY){
                    System.out.print("*\t");
                }else{
                    System.out.printf("%.1f\t",graph.temporaryWeights[x][y]);
                }
            }
            System.out.println();
        }
    }
}
