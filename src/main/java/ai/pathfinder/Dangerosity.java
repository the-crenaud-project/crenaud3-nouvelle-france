package ai.pathfinder;

import datastructures.GPoint2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Dangerosity {
    protected GPoint2D offset;
    protected float width;
    protected float height;
    protected float danger;
    protected float debugMinX,debugMinY,debugMaxX,debugMaxY;
    protected Transport transport=Transport.BOTH;
    public Dangerosity(float width,float height,float danger) {
        this.offset = new GPoint2D(0,0);
        this.width = width > 0 ? width : 0;
        this.height = height > 0 ? height : 0;
        this.danger = danger;
    }
    public Dangerosity setOffset(float offsetX,float offsetY){
        this.offset.setLocation(offsetX,offsetY);
        return this;
    }
    public Dangerosity setTransport(Transport t){
        transport=t;
        return this;
    }
    public Transport getType(){
        return transport;
    }
    public void modifyWeight(Graph toEdit, float x, float y, float angle, boolean tmp){
        modifyWeight(toEdit,x,y,angle,tmp,this.danger);
    }
    
    public float getWidth(){
        return width;
    }

    public float getHeight(){
        return height;
    }

    public GPoint2D getCorner(DangerosityModifiable dm){
        //find the new center of the box
        GPoint2D newCenter=transform(getTransformation((float)dm.getDangerosityPosition().getX(),(float)dm.getDangerosityPosition().getY(),dm.getAngle()),offset);
        //generate de non transformed box
        float width2=width*0.5f,height2=height*0.5f;
        GPoint2D corner[]=new GPoint2D[]{new GPoint2D(width2,height2),
                new GPoint2D(width2,-height2),
                new GPoint2D(-width2,-height2),
                new GPoint2D(-width2,height2)};
        //transform the position of the box and find a frame witch include all corner of the box (min and max values)
        float minX=Float.POSITIVE_INFINITY,minY=Float.POSITIVE_INFINITY,maxX=Float.NEGATIVE_INFINITY,maxY=Float.NEGATIVE_INFINITY;
        float[][] trans=getTransformation((float)dm.getDangerosityPosition().getX(),(float)dm.getDangerosityPosition().getY(),dm.getAngle()),offset;
        for(GPoint2D p : corner){
            p=transform(trans,p);
            if(p.getX()<minX){
                minX=(float)p.getX();
            }if(p.getX()>maxX){
                maxX=(float)p.getX();
            }
            if(p.getY()<minY){
                minY=(float)p.getY();
            }if(p.getY()>maxY){
                maxY=(float)p.getY();
            }
        }
        return new GPoint2D(minX,minY);
    }

    public void modifyWeight(Graph toEdit,float x,float y,float angle,boolean tmp,float danger){
        //find the new center of the box
        GPoint2D newCenter=transform(getTransformation(x,y,angle),offset);
        //generate de non transformed box
        float width2=width*0.5f,height2=height*0.5f;
        GPoint2D corner[]=new GPoint2D[]{new GPoint2D(width2,height2),
                new GPoint2D(width2,-height2),
                new GPoint2D(-width2,-height2),
                new GPoint2D(-width2,height2)};
        //transform the position of the box and find a frame witch include all corner of the box (min and max values)
        float minX=Float.POSITIVE_INFINITY,minY=Float.POSITIVE_INFINITY,maxX=Float.NEGATIVE_INFINITY,maxY=Float.NEGATIVE_INFINITY;
        float[][] trans=getTransformation((float)newCenter.getX(),(float)newCenter.getY(),angle);
        for(GPoint2D p : corner){
            p=transform(trans,p);
            if(p.getX()<minX){
                minX=(float)p.getX();
            }if(p.getX()>maxX){
                maxX=(float)p.getX();
            }
            if(p.getY()<minY){
                minY=(float)p.getY();
            }if(p.getY()>maxY){
                maxY=(float)p.getY();
            }
        }
        debugMinX=minX;debugMaxX=maxX;debugMinY=minY;debugMaxY=maxY;
        //testing if the center of the case are inside the box or not
        int x0=Math.round(minX/toEdit.getNodeWidth())>=0?Math.round(minX/toEdit.getNodeWidth()):0,xn=Math.round(maxX/toEdit.getNodeWidth())<=toEdit.getWidth()?Math.round(maxX/toEdit.getNodeWidth()):toEdit.getWidth(),
                y0=Math.round(minY/toEdit.getNodeWidth())>=0?Math.round(minY/toEdit.getNodeWidth()):0,yn=Math.round(maxY/toEdit.getNodeWidth())<=toEdit.getHeight()?Math.round(maxY/toEdit.getNodeWidth()):toEdit.getHeight();//define iterator limit
        //System.out.println(x0+" "+xn+" "+y0+" "+yn);
        for(int ix=x0;ix<xn;ix++){
            for(int iy=y0;iy<yn;iy++){//simplified because on our case, we can be oriented in only 4 directions
                toEdit.setNodeWeight(ix,iy,danger,tmp);
            }
        }
    }

    protected static GPoint2D transform(float[][] mat,GPoint2D p){
        return new GPoint2D(mat[0][0]*p.getX()+mat[0][1]*p.getY()+mat[0][2],
                mat[1][0]*p.getX()+mat[1][1]*p.getY()+mat[1][2]);
    }

    protected static float[][] getTransformation(float dx,float dy,float da){
        float cos=(float) Math.cos(da),sin=(float) Math.sin(da);
        return new float[][]{{cos,-sin,dx}, {sin,cos,dy}};
    }

    public static void drawDebug(GraphicsContext gc){
        float caseSize= (float) (gc.getCanvas().getWidth()/20.0);
        Graph g=new Graph(20,20,1.0f);
        Dangerosity d=new Dangerosity(6.0f,5.0f,10.0f).setOffset(1.0f,2.0f);
        Dangerosity d2=new Dangerosity(10.0f,5.0f,10.0f).setOffset(15.0f,2.0f);
        float posX=5.5f,posY=6.0f,angle=0;//(float) -(Math.PI/2);
        d.modifyWeight(g,posX,posY,angle,false);
        d2.modifyWeight(g,posX,posY,angle,false);
        float w=2;
        gc.setStroke(Color.BLACK);
        gc.setLineWidth(w);
        for(int x=0;x<20;x++){
            for(int y=0;y<20;y++){
                gc.setFill(g.weights[x][y]==Float.POSITIVE_INFINITY? Color.RED:Color.color(1.0f-((g.weights[x][y]-1.0f)/9.0f),1.0f-((g.weights[x][y]-1.0f)/9.0f),1.0f-((g.weights[x][y]-1.0f)/9.0f)));
                gc.fillRect(caseSize*x,caseSize*y,caseSize,caseSize);
                gc.strokeRect(caseSize*x+w/2,caseSize*y+w/2,caseSize-w,caseSize-w);
            }
        }
        gc.setLineWidth(5);
        gc.setStroke(Color.GREEN);
        //System.out.println(d.debugMinX+" "+d.debugMaxX+" "+d.debugMinY+" "+d.debugMaxY);
        gc.strokeRect(caseSize*d.debugMinX,caseSize*d.debugMinY,caseSize*(d.debugMaxX-d.debugMinX),caseSize*(d.debugMaxY-d.debugMinY));
        gc.setFill(Color.BLUE);
        gc.fillOval(posX*caseSize-5,posY*caseSize-5,10,10);
    }
}
