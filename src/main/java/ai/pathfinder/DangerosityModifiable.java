package ai.pathfinder;

import datastructures.GPoint2D;

import java.util.List;

public interface DangerosityModifiable {
    public GPoint2D getDangerosityPosition();
    public float getAngle();
    public List<Dangerosity> getDangerosities();
    public boolean isTemporary();
}
