package ai.decisionmaker;

import datastructures.GPoint2D;

public interface Building {
    public BuildingType getBuildingType();
    public GPoint2D getEntryPosition();
}
