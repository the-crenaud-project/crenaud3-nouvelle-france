package ai.decisionmaker;

public class Task {
    protected int level=0;
    protected BuildingType building;
    protected boolean interuptable;
    protected Runnable callback;
    public Task(int level,BuildingType bt){
        this.level=level;
        this.interuptable=false;
        this.building=bt;
        this.callback=null;
    }
    public Task(int level,BuildingType bt,Runnable callback){
        this.level=level;
        this.interuptable=false;
        this.building=bt;
        this.callback=callback;
    }
    public void setCallback(Runnable callback){
        this.callback=callback;
    }

    protected void done(){
        if(this.callback!=null){
            this.callback.run();
        }
    }

    public boolean isInteruptable(){
        return this.interuptable;
    }

    public Task setInteruptable(boolean i){
        this.interuptable=i;
        return this;
    }

    public int getLevel(){
        return this.level;
    }

    public void setLevel(int level){
        this.level = level;
    }

    public BuildingType getTarget(){
        return this.building;
    }

    public String toString(){
        return "Task :\nBuildingType : " + building;
    }
}
