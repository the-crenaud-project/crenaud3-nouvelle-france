package ai.decisionmaker;

import ai.AIEngine;
import ai.pathfinder.Transport;
import datastructures.GPoint2D;
import physics.Direction;

import java.util.LinkedList;
import java.util.List;

public class StateMachine {
    public List<Task> tasks;
    protected Task defaultTask;
    protected Task currentTask;
    protected List<GPoint2D> path;
    protected Transport type;
    public StateMachine(Task defaultTask, Transport t){
        this.path=new LinkedList<GPoint2D>();
        this.defaultTask=defaultTask;
        this.tasks=new LinkedList<Task>();
        this.currentTask=null;
        this.type=t;
    }
    public void addTask(Task t){
        this.tasks.add(t);
    }
    public Task getCurrentTask(){
        return currentTask;
    }
    public Direction getTarget(GPoint2D currentPos, float validationRadius){
        if(path!=null && path.size()>0){
            if (Math.abs(currentPos.getX()-path.get(0).getX())+Math.abs(currentPos.getY()-path.get(0).getY()) < 2*validationRadius) {//update the stack
                //System.out.println("remove waypoint!!!");
                path.remove(0);
            }
            if(path.size()>0){//find the next direction
                GPoint2D p=path.get(0);
                //System.out.printf("%.2f;%.2f => %.2f;%.2f\n",currentPos.getX(),currentPos.getY(),p.getX(),p.getY());
                if((Math.abs(currentPos.getX()-p.getX()))>validationRadius) {
                    if (currentPos.getX() > p.getX()) {
                        return Direction.LEFT;
                    }
                    if (currentPos.getX() < p.getX()) {
                        return Direction.RIGHT;
                    }
                }
                if((Math.abs(currentPos.getY()-p.getY()))>validationRadius) {
                    if(currentPos.getY()>p.getY()){
                        return Direction.UP;
                    }
                    if(currentPos.getY()<p.getY()){
                        return Direction.DOWN;
                    }
                }
                //return path.get(0);
            }
        }
        return null;
    }
    public void doTask(BuildingType building){
        if(currentTask!=null){
            if(currentTask.getTarget()==building){
                currentTask.done();
                currentTask=null;
            }
        }
    }
    public void updateState(AIEngine engin, GPoint2D position, float knowledgeLevel){
        Task previous=currentTask;
        Task highestLevel=defaultTask;
        for(Task t : this.tasks){
            if(t.getLevel()>highestLevel.getLevel()){
                highestLevel=t;
            }
        }
        if(currentTask==null || (highestLevel!=currentTask && currentTask.isInteruptable())){
            currentTask=highestLevel;
        }
        if(currentTask!=previous){
            //update path
            float minDist=Float.POSITIVE_INFINITY;//find the closest building
            Building closest=null;
            for(Building b : engin.getBuilding()){
                if(currentTask.getTarget()==b.getBuildingType()){
                    float dist=(float)position.distance(b.getEntryPosition());
                    if(dist<minDist){
                        minDist=dist;
                        closest=b;
                    }
                }
            }
            if(closest!=null){//find a path
                path=engin.getPath(position,closest.getEntryPosition(),knowledgeLevel,this.type);
            }
        }
    }
}
