package ai.decisionmaker;

import java.util.Random;

public enum BuildingType {

    CANTEEN,
    DORMITORY,
    SHOOTINGRANGE,
    TEST;



    public String toString(){
        switch (this){
            case CANTEEN:
                return "Cantine \nUn bâtiment permettant aux soldats de manger\n";
            case DORMITORY:
                return "Dortoir\nUn bâtiment permettant aux soldats de dormir\n";
            case SHOOTINGRANGE:
                return "Stand de tir\nUn bâtiment permettant aux soldats de s'entrainer au tir\n";
            default:
                return "-";
        }
    }

    public static BuildingType getRandomBuildingType(){
        int x = new Random().nextInt(BuildingType.class.getEnumConstants().length - 1);
        return BuildingType.class.getEnumConstants()[x];
    }
}
