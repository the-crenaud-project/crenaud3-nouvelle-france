package ai;

import ai.decisionmaker.Building;
import ai.decisionmaker.BuildingType;
import ai.pathfinder.Dangerosity;
import ai.pathfinder.DangerosityModifiable;
import ai.pathfinder.Transport;
import datastructures.GPoint2D;

import java.util.LinkedList;
import java.util.List;

public class TestBuilding implements Building,DangerosityModifiable {

    protected GPoint2D pos=new GPoint2D();
    protected float angle=0;
    protected boolean temporary=false;
    protected Transport transport=Transport.PEDESTRIAN;
    protected List<Dangerosity> danger=new LinkedList<Dangerosity>();

    protected GPoint2D entry;
    protected BuildingType type;
    public TestBuilding(){
        entry=new GPoint2D();
        type=BuildingType.TEST;
    }
    public BuildingType getBuildingType(){
        return type;
    }
    public GPoint2D getEntryPosition(){
        return entry;
    }

    public List<Dangerosity> getDangerosities(){
        return danger;
    }
    public boolean isTemporary(){
        return temporary;
    }
    public Transport getType(){
        return transport;
    }
    public GPoint2D getDangerosityPosition() {
        return pos;
    }
    public float getAngle() {
        return angle;
    }
}
