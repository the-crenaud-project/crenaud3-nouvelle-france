package ai;

import ai.pathfinder.Dangerosity;
import ai.pathfinder.DangerosityModifiable;
import ai.pathfinder.Transport;
import datastructures.GPoint2D;

import java.util.LinkedList;
import java.util.List;

public class TestDangerosity implements DangerosityModifiable {
    public GPoint2D pos=new GPoint2D();
    public float angle=0;
    public boolean temporary=false;
    public Transport transport=Transport.PEDESTRIAN;
    public List<Dangerosity> danger=new LinkedList<Dangerosity>();
    public TestDangerosity(){
    }
    public List<Dangerosity> getDangerosities(){
        return danger;
    }
    public boolean isTemporary(){
        return temporary;
    }
    public Transport getType(){
        return transport;
    }
    public GPoint2D getDangerosityPosition() {
        return pos;
    }
    public float getAngle() {
        return angle;
    }
}

