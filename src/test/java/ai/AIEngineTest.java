package ai;

import ai.decisionmaker.BuildingType;
import ai.decisionmaker.StateMachine;
import ai.decisionmaker.Task;
import ai.pathfinder.Dangerosity;
import ai.pathfinder.Graph;
import ai.pathfinder.Transport;
import datastructures.GPoint2D;
import org.junit.jupiter.api.Test;

class AIEngineTest {

    @Test
    public void AIEngineBenchmarkAndTest() {
        AIEngine aiEngine=new AIEngine(10,10,0.5f);
        System.out.println("AIEngine size=["+aiEngine.pedestrian.getWidth()+"*"+aiEngine.pedestrian.getHeight()+"] (cell size="+aiEngine.pedestrian.getNodeWidth()+")");

        TestBuilding b1=new TestBuilding();
        b1.entry.setLocation(2,5.4);
        b1.type=BuildingType.CANTEEN;
        b1.angle=1.57f;
        b1.temporary=false;
        b1.transport=Transport.BOTH;
        b1.pos.setLocation(3,5.4);
        b1.danger.add(new Dangerosity(5,3,Float.POSITIVE_INFINITY).setOffset(1,0).setTransport(Transport.VEHICULE));
        b1.danger.add(new Dangerosity(5,3,5).setOffset(1,0).setTransport(Transport.PEDESTRIAN));
        b1.danger.add(new Dangerosity(1,1,2).setOffset(0,-5).setTransport(Transport.BOTH));
        aiEngine.register((Object)b1);

        System.out.println("pedestrian:");
        Graph.drawGraphWeight(aiEngine.pedestrian);
        System.out.println("vehicule:");
        Graph.drawGraphWeight(aiEngine.vehicule);

        StateMachine m1=new StateMachine(new Task(5, BuildingType.CANTEEN),Transport.PEDESTRIAN);
        m1.addTask(new Task(10,BuildingType.TEST,()->{System.out.println("task done!!!!!");}));
        m1.updateState(aiEngine,new GPoint2D(1.0,1.0),0.5f);
        System.out.println("current task go to: "+m1.getCurrentTask().getTarget());
        System.out.println("current target: "+m1.getTarget(new GPoint2D(0,0),0.1f));
        m1.doTask(BuildingType.TEST);
    }

}